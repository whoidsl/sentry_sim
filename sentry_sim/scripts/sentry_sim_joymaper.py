#!/usr/bin/env python
# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import rospy
import math
import argparse
import sys

from sensor_msgs.msg import Joy, JointState
from ds_core_msgs.msg import Abort
from ds_actuator_msgs.msg import ServoCmd, ThrusterCmd
from ds_actuator_msgs.srv import XrCmd
import std_msgs.msg
import time

class SentryState(object):
    def __init__(self, no_xr):

        # Everything is normalized by time so its timer-independent
        self.timer_interval = 0.10

        # Thrust / angle commands
        # (clamped to +/- 1.0)
        self.surge = 0
        self.sway  = 0
        self.hdg   = 0
        self.fwd_angle = 0
        self.aft_angle = 0
        self.abort = False
        self.enable = True
        self.mode = "cmd"

        # Remapping 
        self.motor_limit = 10
        self.servo_limit = math.radians(120)
        
        # Gains
        self.servo_gain = math.radians(15) / self.servo_limit
        self.surge_gain = 1.0
        self.sway_gain  = 1.0
        self.hdg_gain   = 1.0

        # Joystick state
        self.joy_surge = 0
        self.joy_sway  = 0
        self.joy_hdg   = 0
        self.joy_fwd   = 0
        self.joy_aft   = 0

        # Outgoing messages
        self._servo_msgs = [ServoCmd() for i in range(2)]
        self._servo_msgs[0].servo_name = 'FWD'
        self._servo_msgs[1].servo_name = 'AFT'
        self._servo_publishers = [rospy.Publisher('actuators/servo_fwd', ServoCmd, queue_size=1),
                                  rospy.Publisher('actuators/servo_aft', ServoCmd, queue_size=1)]

        self._motor_msgs = [ThrusterCmd() for i in range(4)]
        self._motor_publishers = [rospy.Publisher('actuators/thruster_fwd_port', ThrusterCmd, queue_size=1),
                                  rospy.Publisher('actuators/thruster_fwd_stbd', ThrusterCmd, queue_size=1),
                                  rospy.Publisher('actuators/thruster_aft_port', ThrusterCmd, queue_size=1),
                                  rospy.Publisher('actuators/thruster_aft_stbd', ThrusterCmd, queue_size=1)]

        self._joint_state_msg = JointState()
        self._joint_state_publisher = rospy.Publisher('joint_states', JointState, queue_size=1)

        self._abort_publisher = rospy.Publisher('abort', Abort, queue_size=1)
        self._abort_msg = Abort()

        self._last_drop_time = 0
        self._xr1_service = None
        self._xr2_service = None
        if not no_xr:
            rospy.wait_for_service('actuators/xr1/cmd')
            rospy.wait_for_service('actuators/xr2/cmd')
            self._xr1_service = rospy.ServiceProxy('actuators/xr1/cmd', XrCmd)
            self._xr2_service = rospy.ServiceProxy('actuators/xr2/cmd', XrCmd)

    def on_timer(self, event):
        self.surge = self.saturate(self.surge_gain * self.joy_surge, 1.0)
        self.sway  = self.saturate(self.sway_gain  * self.joy_sway,  1.0)
        self.hdg   = self.saturate(self.hdg_gain   * self.joy_hdg,   1.0)

        self.fwd_angle = self.saturate(self.fwd_angle 
                     + self.servo_gain*self.timer_interval*self.joy_fwd, 1.0)

        self.aft_angle = self.saturate(self.aft_angle 
                     + self.servo_gain*self.timer_interval*self.joy_aft, 1.0)

        #rospy.loginfo(rospy.get_caller_id() 
        #            + (" T: %7.5f H: %7.5f FWD_S: %7.5f AFT_S: %7.5f" 
        #              % (self.surge, self.hdg, 
        #                 math.degrees(self.fwd_angle),
        #                 math.degrees(self.aft_angle))))

        if self.mode == 'joint_state':
            rospy.loginfo(rospy.get_caller_id() + ' Sending joint state')
            self.send_joint_state()

        elif self.mode == 'cmd':
            self.send_cmd()
        else:
            rospy.logerr(rospy.get_caller_id() + ' Unknown mode %s' % self.mode)

        self._abort_msg.abort = self.abort
        self._abort_msg.enable = self.enable
        if self.abort:
            rospy.logerr(rospy.get_caller_id() + ' ABORT ABORT ABORT!')
        self._abort_msg.ttl = 30
        self._abort_publisher.publish(self._abort_msg)

    def on_joymsg(self, joydata):
        # Major drive axes
        #self.joy_sway  = joydata.axes[0]
        self.joy_hdg   = joydata.axes[0]
        self.joy_surge = joydata.axes[1]

        # Set up the fin angle controls
        if joydata.buttons[4]:
            # Left-bumper
            self.joy_fwd  = joydata.axes[4]

        if joydata.buttons[5]:
            # Right-bumper
            self.joy_aft  = joydata.axes[4]

        if not joydata.buttons[4] and not joydata.buttons[5]:
            self.joy_fwd  = joydata.axes[4]
            self.joy_aft  = joydata.axes[4]

        # Zero the fin angles
        if joydata.buttons[3]:
            self.fwd_angle = 0
            self.aft_angle = 0

        # Enable toggle
        if joydata.buttons[1]:
            # B
            if self.enable:
                self.enable = False
            else:
                self.enable = True

        # Drop descent weights
        if joydata.buttons[2]:
            # X
            self.drop_descent()
        
        # Mode button (start)
        if joydata.buttons[7]:
            if self.mode == 'joint_state':
                self.mode = 'cmd'
            else:
                self.mode = 'joint_state'
            rospy.loginfo(rospy.get_caller_id() + ' Mode= ' + self.mode)

        # Process the triggers for abort
        if joydata.axes[2] < -0.5 and joydata.axes[5] < -0.5:
            self.abort = True
        else:
            self.abort = False

    def send_joint_state(self):
        self._joint_state_msg.header.stamp = rospy.Time.now()
        self._joint_state_msg.header.seq += 1
        self._joint_state_msg.name = ['fwd_servo', 'aft_servo']
        self._joint_state_msg.position = [self.servo_limit * self.fwd_angle,
                                          self.servo_limit * self.aft_angle]
        self._joint_state_publisher.publish(self._joint_state_msg)

    def send_cmd(self):
        toPrint = self.send_servos()
        toPrint += self.send_motors()

        #rospy.loginfo(rospy.get_caller_id() + ' ' + toPrint)

    def drop_descent(self):
        if time.time() - self._last_drop_time < 5:
            return
        self._last_drop_time = time.time()

        rospy.loginfo(rospy.get_caller_id() + ' DROP DESCENT!!!')

        if self._xr2_service is None:
            rospy.logerr(rospy.get_caller_id() + ' NO XR SERVICE AVAILABLE (no sim?)')
            result = None
        else:
            result = self._xr2_service(XrCmd._request_class.XR_FUNCTION_DCAM, 2, 1, False)
        
            rospy.logwarn(rospy.get_caller_id() + ' DESCENT DROP WEIGHT RESULT: ' + result.msg)
            if not result.success:
                rospy.logerr(rospy.get_caller_id() + ' FAILED to drop descent weight!: ' + result.msg)


    def send_servos(self):
        cmds = [self.fwd_angle, self.aft_angle]
        names = ['fwd_servo', 'aft_servo']
        for i in range(2):
            self._servo_msgs[i].stamp = rospy.Time.now()
            self._servo_msgs[i].servo_name = names[i]
            self._servo_msgs[i].cmd_radians = self.servo_limit * cmds[i]
            self._servo_publishers[i].publish(self._servo_msgs[i])

        return ("F:%10.2f A:%10.2f " % (math.degrees(self._servo_msgs[0].cmd_radians), 
                                       math.degrees(self._servo_msgs[1].cmd_radians)))


    def send_motors(self):
        cmds = [self.saturate(self.surge - self.hdg, 1.0),
                self.saturate(self.surge + self.hdg, 1.0),
                self.saturate(self.surge - self.hdg, 1.0),
                self.saturate(self.surge + self.hdg, 1.0)]

        ret = ''
        for i in range(4):
            self._motor_msgs[i].stamp = rospy.Time.now()
            self._motor_msgs[i].cmd_value = self.motor_limit * cmds[i]
            self._motor_publishers[i].publish(self._motor_msgs[i])
            ret += ('%d:%8.4f ' % (i+1, self._motor_msgs[i].cmd_value))
        return ret

    def saturate(self, val, limit):
        return math.copysign(min(abs(val), limit), val)

        
def listener(argv = None):

    if argv is None:
        argv = sys.argv

    # filter out extra arguments
    argv = rospy.myargv(argv)

    parser = argparse.ArgumentParser(description='Simulated Sentry actuator commands')
    parser.add_argument('--no-xr', '-x', action='store_true', help='Don''t use the XR command service')
    args = parser.parse_args(args=argv[1:])

 
    rospy.init_node('sentry_sim_joymapper')
    state = SentryState(args.no_xr)

    rospy.Subscriber("joy", Joy, state.on_joymsg)
    rospy.Timer(rospy.Duration(state.timer_interval), state.on_timer)

    rospy.spin()

if __name__ == '__main__':
    listener()
