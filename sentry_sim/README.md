# ds_sim

Vehicle-specific Sentry tools, urdf, etc

## Contents of this Package

Mostly just a joystick remapper

## Getting Started

This is a DS ROS package intended to be run as part of the larger DS ROS echosystem.
See the [Sentry Wiki](http://sentry-wiki.whoi.edu/ROS_Upgrade)

### Prerequisites

This package requires ds_sim, ds_msgs and gazebo.  These (and any other) dependencies should be managed correctly in the package.xml.

### Installing

You should generally be installing this as part of a rosinstall file.  
If handled seperately though, simply add to a catkin workspace and 
build with:

```
catkin_make
```

## Deployment

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Ian Vaughn** - *Initial work* - [WHOI email](mailto:ivaughn@whoi.edu)
* **Stefano Suman** - *Initial work* - [WHOI email](mailto:ssuman@whoi.edu)

## Acknowledgments

* IFREMER for their architectural support
* Louis Whitcomb et. al. for his message definitions to look over
* Zac Berkowitz for his intial work on URDF that was shamelessly duplicated here
* Justin Fujii for his EXCELLENT meshes


