/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Ian Vaughn, October 2017
// 
// Based on the simplified 4-DOF dynamics model
// in ROV

//#include <sentry_sim/SentryDynamics_plugin.hh>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/subscribe_options.h>
#include <ds_actuator_msgs/ServoCmd.h>
#include <ds_actuator_msgs/ServoState.h>
#include <ds_actuator_msgs/ThrusterCmd.h>
#include <ds_actuator_msgs/ThrusterState.h>
#include <ds_core_msgs/Abort.h>

#include <ds_sim/common.hpp>

#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Accel.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/WrenchStamped.h>

#include <sentry_msgs/SimState.h>

#include <boost/bind.hpp>
#include <tf/transform_datatypes.h>
#include <sensor_msgs/JointState.h>
#include <ds_hotel_msgs/XrSimState.h>
#include <sentry_msgs/XRCmd.h>
#include <ds_hotel_msgs/XR.h>
#include <algorithm>

bool loadString(std::string &result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin sentrydynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<std::string>(tag);

    // remove any double-quotes
    result.erase(std::remove(result.begin(), result.end(), '\"'),result.end());

    ROS_INFO_STREAM("ROS plugin sentrydynamics/servo load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

bool loadDouble(double& result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin sentrydynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<double>(tag);

    ROS_INFO_STREAM("ROS plugin sentrydynamics/servo load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

bool loadInt(int& result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin sentrydynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<int>(tag);

    ROS_INFO_STREAM("ROS plugin sentrydynamics/servo load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}


class VehicleModel {
  public:
    
    void Load(gazebo::physics::ModelPtr _parent, sdf::ElementPtr _sdf) {
        loadDouble(CS_AREA, _sdf, "CS_Area");
        loadDouble(CS_RHO,  _sdf, "CS_Rho");
        loadDouble(CS_Cl0, _sdf, "CS_Cl0");
        loadDouble(CS_Cl1, _sdf, "CS_Cl1");
        loadDouble(CS_Cl2, _sdf, "CS_Cl2");
        loadDouble(CS_alpha1, _sdf, "CS_alpha1");
        loadDouble(CS_alpha2, _sdf, "CS_alpha2");
        loadDouble(CS_Cd0, _sdf, "CS_Cd0");
        loadDouble(CS_Cd1, _sdf, "CS_Cd1");
        loadDouble(CS_Cd2, _sdf, "CS_Cd2");
        loadDouble(Mu, _sdf, "Mu");
        loadDouble(Cdu, _sdf, "Cdu");
        loadDouble(Mw, _sdf, "Mw");
        loadDouble(Cdw, _sdf, "Cdw");
        loadDouble(Izz, _sdf, "Izz");
        loadDouble(Cdr, _sdf, "Cdr");

        loadDouble(thrust_to_amps_Alpha, _sdf, "thrust_to_amps_alpha");
        loadDouble(thrust_to_amps_Beta,  _sdf, "thrust_to_amps_beta");
    }

    double calcLift(double aoa, double u) const {
        double Cl, abs_aoa, result;
        abs_aoa = fabs(aoa);
        Cl = CS_Cl0*sin(2.0*abs_aoa);

        if (abs_aoa <= CS_alpha1) {
            Cl += CS_Cl1*abs_aoa;
        }

        if (abs_aoa >= CS_alpha2) {
            Cl += CS_Cl2*(M_PI-abs_aoa);
        }

        result = 0.5 * CS_RHO*CS_AREA*Cl*u*u;
        if (aoa < 0.0) {
            result = -result;
        }
        return result;
    }

    double calcDrag(double aoa, double u) const {
        double Cd, abs_aoa, da;
        abs_aoa = fabs(aoa);
        da = M_PI/2.0 - abs_aoa;
        Cd = CS_Cd0 * exp( -(da*da)/(2.0*CS_Cd1*CS_Cd1))
             * CS_Cd2 *(1.0-cos(4.0*abs_aoa));
        return 0.5 * CS_RHO*CS_AREA*Cd*u*u;
    }

    double getMu()  const { return  Mu; };
    double getCdu() const { return Cdu; };

    double getMw()  const { return  Mw; };
    double getCdw() const { return Cdw; };

    double getIzz() const { return Izz; };
    double getCdr() const { return Cdr; };

    double getThrustToAmps_Alpha() const { return thrust_to_amps_Alpha; };
    double getThrustToAmps_Beta()  const { return thrust_to_amps_Beta; };


  protected:
    // Surge parameters
    double Mu, Cdu;

    // Heave parameters
    double Mw, Cdw;

    // Rotation parameters
    double Izz, Cdr;

    // Control surface Lift
    double CS_Cl0, CS_Cl1, CS_Cl2;
    double CS_alpha1, CS_alpha2;

    // Control surface Drag model
    double CS_Cd0, CS_Cd1, CS_Cd2;

    // global parameters
    double CS_AREA, CS_RHO;

    // thruster parameters
    double thrust_to_amps_Alpha, thrust_to_amps_Beta;
};

// forward declaration
class SentryDynamics;

template <typename COMMAND_T, typename STATE_T, typename SIM_T>
class SentryDevice {
  public:
    void Load(VehicleModel* _vehicle, const std::unique_ptr<ros::NodeHandle>& rosNode,
              gazebo::physics::ModelPtr _parent, sdf::ElementPtr _sdf) {


        if (!_sdf) {
            return;
        }

        vehicle = _vehicle;
	#if GAZEBO_MAJOR_VERSION > 7
            force_xyz  = ignition::math::Vector3d(0.0, 0.0, 0.0);
            torque_rph = ignition::math::Vector3d(0.0, 0.0, 0.0);
            force_local_xyz  = ignition::math::Vector3d(0.0, 0.0, 0.0);
            torque_local_rph = ignition::math::Vector3d(0.0, 0.0, 0.0);
	#else
	    force_xyz  = gazebo::math::Vector3(0.0, 0.0, 0.0);
            torque_rph = gazebo::math::Vector3(0.0, 0.0, 0.0);
            force_local_xyz  = gazebo::math::Vector3(0.0, 0.0, 0.0);
            torque_local_rph = gazebo::math::Vector3(0.0, 0.0, 0.0);
	#endif

        // load basic parameters
        std::string root_topic_name;
        if (!( loadString(root_topic_name, _sdf, "root_topic")
            && loadString(local_link, _sdf, "linkname")
            && loadString(sim_param_ns, _sdf, "sim_params")
            && loadString(body_link , _sdf->GetParent(), "linkname"))) {
            return;
        }

        // Setup the update rate
        double updateRate = 10;
        if (_sdf->HasElement("updateRate")) {
            updateRate = _sdf->Get<double>("updateRate");
        }
        rosPublishPeriod = gazebo::common::Time(1.0/updateRate);
        lastRosPublished = gazebo::common::Time(0.0);

        // initialize our state variable
        state.state.header.stamp = ros::Time().now();
        state.state.ds_header.io_time = state.state.header.stamp;

        if (rosNode->hasParam(sim_param_ns + "/robot_in_loop") &&
               rosNode->getParam(sim_param_ns + "/robot_in_loop", hardware_in_loop)) {
            ROS_INFO_STREAM("Loaded robot_in_loop=" <<hardware_in_loop);
        } else {
            ROS_ERROR_STREAM("Unable to load robot_in_loop from " <<rosNode->resolveName(sim_param_ns + "/robot_in_loop"));
            hardware_in_loop = false;
        }

        // Setup ROS subscription / advertisement
        // NOTE: This works because a boost::bind to a virtual function will
        // do the whole virtual function lookup thing at runtime.  Its like..
        // guaranteed and stuff.
        rosSub = rosNode->subscribe<COMMAND_T>(root_topic_name + "/cmd", 1,
                    boost::bind(&SentryDevice::OnCommand, this, _1));
        rosAbortSub = rosNode->subscribe<ds_core_msgs::Abort>("abort", 1,
                    boost::bind(&SentryDevice::OnAbortMsg, this, _1));
        if (!hardware_in_loop) {
          rosPub = rosNode->advertise<STATE_T>(root_topic_name + "/state", 10);
        }

        wrenchBase  = rosNode->advertise<geometry_msgs::WrenchStamped>("sim/" + root_topic_name + "/wrench_body", 10);
        wrenchLocal = rosNode->advertise<geometry_msgs::WrenchStamped>("sim/" + root_topic_name + "/wrench_local", 10);
    }

    virtual void OnCommand(const boost::shared_ptr<const COMMAND_T>& _msg) = 0;

    void OnAbortMsg(const ds_core_msgs::Abort::ConstPtr &_msg) {
        state.state.enable = _msg->enable;
    }

    void Publish(const gazebo::common::UpdateInfo& _info) {
        if (_info.simTime > lastRosPublished + rosPublishPeriod) {
            // publish a state message
            if (!hardware_in_loop) {
              state.state.header.stamp = ros::Time().now();
              state.state.ds_header.io_time = state.state.header.stamp;
              rosPub.publish(state.state);
            }

            lastRosPublished = _info.simTime;

            // Now fill in the wrench messages
            geometry_msgs::WrenchStamped msg;
            msg.header = state.state.header;

            msg.header.frame_id = local_link;
            fillWrench(msg.wrench, force_local_xyz, torque_local_rph);
            wrenchLocal.publish(msg);

            msg.header.frame_id = body_link;
            fillWrenchFossen(msg.wrench, force_xyz, torque_rph);
            wrenchBase.publish(msg);
        }
    }


    const SIM_T& getSimState() const { return state; };

    #if GAZEBO_MAJOR_VERSION > 7
        const ignition::math::Vector3d& getForceXYZ() const {
            return force_xyz;
        }
    #else
        const gazebo::math::Vector3& getForceXYZ() const {
            return force_xyz;
        }
    #endif

    #if GAZEBO_MAJOR_VERSION > 7
        const ignition::math::Vector3d& getTorqueRPH() const {
            return torque_rph;
        }
    #else
        const gazebo::math::Vector3& getTorqueRPH() const {
            return torque_rph;
        }
    #endif

  protected:
    SIM_T state;
    gazebo::common::Time rosPublishPeriod, lastRosPublished;
    VehicleModel* vehicle;
  
    #if GAZEBO_MAJOR_VERSION > 7
        ignition::math::Vector3d force_xyz, torque_rph;
        ignition::math::Vector3d force_local_xyz, torque_local_rph;
    #else
        gazebo::math::Vector3 force_xyz, torque_rph;
        gazebo::math::Vector3 force_local_xyz, torque_local_rph;
    #endif
  
    std::string local_link;
    std::string body_link;
 
    std::string sim_param_ns;

    bool hardware_in_loop;

    ros::Subscriber rosSub;
    ros::Subscriber rosAbortSub;
    ros::Publisher rosPub;
    ros::Publisher wrenchBase;
    ros::Publisher wrenchLocal;
};

class SentryServo : public SentryDevice<ds_actuator_msgs::ServoCmd, ds_actuator_msgs::ServoState, sentry_msgs::SimServo> {
    typedef SentryDevice<ds_actuator_msgs::ServoCmd, ds_actuator_msgs::ServoState, sentry_msgs::SimServo> SuperClass;
  public:
    void Load(VehicleModel* _vehicle, const std::unique_ptr<ros::NodeHandle>& rosNode, 
              gazebo::physics::ModelPtr _parent, sdf::ElementPtr _sdf) {

        // superclass load!
        SuperClass::Load(_vehicle, rosNode, _parent, _sdf);
        if (!_sdf) {
            ROS_FATAL_STREAM("SentryThruster Plugin has NULL SDF! (sentry.urdf error?)");
            return;
        }

        // connect to the correct joint
        std::string joint_name;
        if (!loadString(joint_name, _sdf, "joint_name")) {
            return;
        }
        joint = _parent->GetJoint(joint_name);
        if (!joint) {
            ROS_FATAL_STREAM("ROS/Gazebo Servo plugin couldn't find joint_name \"" <<joint_name <<"\"");
            return;
        }
        state.state.servo_name = joint_name;

        if (hardware_in_loop) {
          ROS_ERROR_STREAM("USING hardware in-the-loop");
          rosJointSub = rosNode->subscribe("joint_states", 10, &SentryServo::OnJointstate, this);
        } else {
          ROS_ERROR_STREAM("NOT using hardware in-the-loop");
        }

    }

    void OnJointstate(sensor_msgs::JointStateConstPtr joint_msg) {
        for (size_t i=0; i<joint_msg->name.size(); i++) {
          if (joint_msg->name[i] != state.state.servo_name) {
            continue;
          }

          //ROS_ERROR_STREAM("Setting joint position to " <<joint_msg->position[i]);
          // YAY! It's our joint!
          joint->SetPosition(0, joint_msg->position[i]);
          joint->SetVelocity(0,0);
          joint->SetForce(0,0);
        }
    }

    virtual void OnCommand(const ds_actuator_msgs::ServoCmd::ConstPtr &_msg) {
        state.state.cmd_radians = _msg->cmd_radians;
        ROS_DEBUG_STREAM("Dsros: Commanded to " <<state.state.cmd_radians*180/M_PI <<" deg");
    }

    void OnUpdate(const gazebo::common::UpdateInfo& _info, double _body_aoa, double _fluid_vel) {
        // do the kinematics
        if (hardware_in_loop) {
	    #if GAZEBO_MAJOR_VERSION > 7
                state.state.actual_radians = joint->Position(0);
	    #else
	        state.state.actual_radians = joint->GetAngle(0).Radian();
	    #endif
        } else if (state.state.enable) {
            state.state.actual_radians = state.state.cmd_radians;

            // update the joint angle
            joint->SetPosition(0,state.state.actual_radians);
            joint->SetVelocity(0,0);
            joint->SetForce(0,0);
        }

        state.fluid_vel = _fluid_vel;
        state.fluid_aoa = _body_aoa + state.state.actual_radians;
        state.lift = vehicle->calcLift(state.fluid_aoa, state.fluid_vel);
        state.drag = vehicle->calcDrag(state.fluid_aoa, state.fluid_vel);

        // Actually compute the force
        // transform drag & lift into the BODY coordinate system based on the _fluid_aoa
        force_xyz.Set(-(state.drag*cos(_body_aoa) - state.lift*sin(_body_aoa)),
                       0,
                      -(state.drag*sin(_body_aoa) + state.lift*cos(_body_aoa)));
        force_local_xyz.Set(-state.drag*cos(state.state.actual_radians) + state.lift*sin(state.state.actual_radians),
                            0,
                            -state.drag*sin(state.state.actual_radians) - state.lift*cos(state.state.actual_radians));

        ROS_DEBUG_STREAM("Dsros: Setting servo to angle=" <<state.state.actual_radians*180/M_PI <<" deg");

        SuperClass::Publish(_info);
    }

    double getAngle() const {
        return state.state.actual_radians;
    }

  protected:
    gazebo::physics::JointPtr joint;
    ros::Subscriber rosJointSub;
};


class SentryThruster : public SentryDevice<ds_actuator_msgs::ThrusterCmd, ds_actuator_msgs::ThrusterState, sentry_msgs::SimThruster> {
    typedef SentryDevice<ds_actuator_msgs::ThrusterCmd, ds_actuator_msgs::ThrusterState, sentry_msgs::SimThruster> SuperClass;
  public:

    SentryThruster() {
        servo = NULL;
    }

    void Load(VehicleModel* _vehicle, const std::unique_ptr<ros::NodeHandle>& rosNode, 
              gazebo::physics::ModelPtr _parent, sdf::ElementPtr _sdf) {

        // superclass load!
        SuperClass::Load(_vehicle, rosNode, _parent, _sdf);
        if (!_sdf) {
            ROS_FATAL_STREAM("SentryThruster Plugin has NULL SDF! (sentry.urdf error?)");
            return;
        }

        if (!( loadDouble(gain, _sdf, "gain") 
            && loadDouble(moment_arm, _sdf, "moment_arm"))) {
            return;
        }
        
        // Load the thruster name
        thruster_name = _sdf->GetName();
        state.state.thruster_name = thruster_name; // TODO: Rename me in the msg file!
    }

    void OnUpdate(const gazebo::common::UpdateInfo& _info, double _fluid_aoa, double _fluid_vel) {
        if (!servo) {
            ROS_ERROR_STREAM("SentryThruster: No servo associated with thruster " <<thruster_name);
            return;
        }

        // do the kinematics
        if (state.state.enable) {
            state.state.actual_value = state.state.cmd_value;
        } else {
            state.state.actual_value = 0;
        }

        //ROS_DEBUG_STREAM("SentryThruster: Setting thruster " <<thruster_name <<" to " <<state.state.actual_value);

        // compute fluid-flow along the thruster axis
        double servo_angle = servo->getAngle();
        state.fluid_u = fabs(cos(_fluid_aoa) * _fluid_vel);

        // Positive current == Forward Force
        state.thrust = gain * state.state.actual_value * (vehicle->getThrustToAmps_Alpha()
                                         + vehicle->getThrustToAmps_Beta() * state.fluid_u);

        force_xyz.Set( state.thrust*cos(servo_angle),
                        0,
                       -state.thrust*sin(servo_angle));
        torque_rph.Set( 0,
                        0,
                        state.thrust*cos(servo_angle)*moment_arm);

        // Torque comes from teh transform from thruster to body
        force_local_xyz.Set(state.thrust, 0, 0);
        torque_local_rph.Set(0,0,0);

        // publish!
        SuperClass::Publish(_info);

    }

    virtual void OnCommand(const ds_actuator_msgs::ThrusterCmd::ConstPtr &_msg) {
        state.state.cmd_value = _msg->cmd_value;
        ROS_DEBUG_STREAM("SentryThruster: Commanded thruster \"" <<thruster_name <<"\" to " <<state.state.cmd_value);
    }

    void setServo(SentryServo* _s) { servo = _s; };

  protected:
    std::string thruster_name;
    SentryServo* servo;
    double gain;
    double moment_arm;
};



class Xr {
  public:
    void Load(VehicleModel* _vehicle, const std::unique_ptr<ros::NodeHandle>& rosNode, 
              gazebo::physics::ModelPtr _parent, sdf::ElementPtr _sdf) {

        std::string root_topic_name;
        int idx;
        if (!( loadAttribute<int>(idx, _sdf, "idx")
            && loadAttribute(longman, _sdf, "longman")
            && loadAttribute(shortman_reset, _sdf, "shortman")
            && loadAttribute(dcamtime, _sdf, "dcamtime")
            && loadAttribute(burntime, _sdf, "burntime")
            && loadElement(sim_param_ns, _sdf, "sim_params")
            && loadElement(root_topic_name, _sdf, "root_topic"))) {
            return;
        }

        state.idx = static_cast<uint8_t>(idx);
        std::stringstream addr;
        addr <<"GRD/SIM_XR_" <<state.idx;
        sail_address = addr.str();

        // Setup the update rate
        double updateRate = 0.1;
        if (_sdf->HasElement("updateRate")) {
            updateRate = _sdf->Get<double>("updateRate");
        }
        rosPublishPeriod = gazebo::common::Time(1.0/updateRate);
        lastRosPublished = gazebo::common::Time(0.0);


        // initialize our state
        state.dcams_actual.resize(2);
        state.dcams_actual[0] = 1;
        state.dcams_actual[1] = 1;

        state.wires_actual.resize(2);
        state.wires_actual[0] = 0;
        state.wires_actual[1] = 0;

        state.dcams_cmds.resize(2);
        state.dcams_cmds[0] = 1;
        state.dcams_cmds[1] = 1;

        state.wires_cmds.resize(2);
        state.wires_cmds[0] = 0;
        state.wires_cmds[1] = 0;

        state.motor_secs.resize(2);
        state.motor_secs[0] = 0;
        state.motor_secs[1] = 0;

        state.burnwire_secs.resize(2);
        state.burnwire_secs[0] = 0;
        state.burnwire_secs[1] = 0;

        state.short_deadsecs = 255;
        state.deadsecs = 255*3600; // initialize to 255 hours

        state.acoustic_code = 0;

        state.dcam_open_pct.resize(2);
        state.dcam_open_pct[0] = 0.0;
        state.dcam_open_pct[1] = 0.0;
        state.wire_burn_pct.resize(2);
        state.wire_burn_pct[0] = 0.0;
        state.wire_burn_pct[1] = 0.0;

        initializing = true;

        // figure out which weights connect to which burnwire / dcam
        burn_idx.resize(2);
        dcam_idx.resize(2);
        if (!(  loadElement(dcam_idx[0], _sdf, "dcam1")
             && loadElement(dcam_idx[1], _sdf, "dcam2")
             && loadElement(burn_idx[0], _sdf, "burn1")
             && loadElement(burn_idx[1], _sdf, "burn2") )) {
            return;
        }

        if (rosNode->hasParam(sim_param_ns + "/robot_in_loop") &&
               rosNode->getParam(sim_param_ns + "/robot_in_loop", hardware_in_loop)) {
            ROS_INFO_STREAM("XR Loaded robot_in_loop=" <<hardware_in_loop);
        } else {
            ROS_ERROR_STREAM("Unable to load robot_in_loop from " <<rosNode->resolveName(sim_param_ns + "/robot_in_loop"));
            hardware_in_loop = false;
        }

        // prepare to publish!

        if (hardware_in_loop) {
          rosRealXrSub = rosNode->subscribe(root_topic_name + "/state", 10, &Xr::OnRealXrState, this);

        } else {
          // we have to simulate our own hardware.  So, you know.  Do that.
          rosAbortSub = rosNode->subscribe<ds_core_msgs::Abort>("abort", 1,
                    boost::bind(&Xr::OnAbortMsg, this, _1));
          rosPub    = rosNode->advertise<ds_hotel_msgs::XR>(root_topic_name + "/state", 10);

          rosServe  = rosNode->advertiseService(root_topic_name + "/cmd", &Xr::ProcessCommand, this);
        }
        rosSimPub = rosNode->advertise<ds_hotel_msgs::XrSimState>("sim/" + root_topic_name + "/state", 10);
    }

    void ForceOpenDcam(int i) {
      if (i == 0 || i == 1) {
        state.dcam_open_pct[i] = 1.0;
      } else {
        ROS_ERROR_STREAM("Unable to force open dcam# " <<i);
      }
    }

    void OnRealXrState(const ds_hotel_msgs::XR& _msg) {

      // require 2 consecutive messages to open dcams
      if (_msg.motor_1_drop) {
        clamp(state.dcam_open_pct[0] += 50);
      }
      if (_msg.motor_1_hold) {
        clamp(state.dcam_open_pct[0] -= 50);
      }
      if (_msg.motor_2_drop) {
        clamp(state.dcam_open_pct[1] += 50);
      }
      if (_msg.motor_2_hold) {
        clamp(state.dcam_open_pct[1] -= 50);
      }

      // require 10 messages to burn the wires
      if (_msg.burnwire_1_drive) {
        clamp(state.wire_burn_pct[0] += 10);
      }
      if (_msg.burnwire_2_drive) {
        clamp(state.wire_burn_pct[1] += 10);
      }

      // Publish an intneral sim state message in response.
      // Just to be sure
      state.stamp = ros::Time().now();
      rosSimPub.publish(state);

    }

    void OnAbortMsg(const ds_core_msgs::Abort::ConstPtr &_msg) {
        if (_msg->abort) {
          state.short_deadsecs = 0;
          state.motor_secs[0] = 10;
          state.motor_secs[1] = 10;
          state.burnwire_secs[0] = 3600;
          state.burnwire_secs[1] = 3600;

          command_open_all_dcams();
          command_burn_all_wires();
        } else {
            // tickle the shortman deadman
          state.short_deadsecs = shortman_reset;
        }
    }

    bool ProcessCommand(sentry_msgs::XRCmdRequest& req, sentry_msgs::XRCmdResponse& res) {
        // Sanity check
        switch (req.command) {
          case sentry_msgs::XRCmdRequest::XR_CMD_DCAM_OPEN:
            state.dcams_cmds[0] = 1;
            state.dcams_cmds[1] = 1;
            state.motor_secs[0] = 5;
            state.motor_secs[1] = 5;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_DCAM_CLOSE:
            state.dcams_cmds[0] = 0;
            state.dcams_cmds[1] = 0;
            state.motor_secs[0] = 5;
            state.motor_secs[1] = 5;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_BURNWIRE_ON:
            state.wires_cmds[0] = 1;
            state.wires_cmds[1] = 1;
            state.burnwire_secs[0] = 3600;
            state.burnwire_secs[1] = 3600;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_BURNWIRE_OFF:
            state.wires_cmds[0] = 0;
            state.wires_cmds[1] = 0;
            state.burnwire_secs[0] = 3600;
            state.burnwire_secs[1] = 3600;
            break;

          case sentry_msgs::XRCmdRequest::XR_CMD_DCAM_OPEN_1:
            state.dcams_cmds[0] = 1;
            state.motor_secs[0] = 5;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_DCAM_CLOSE_1:
            state.dcams_cmds[0] = 0;
            state.motor_secs[0] = 5;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_BURNWIRE_ON_1:
            state.wires_cmds[0] = 1;
            state.wires_actual[0] = 0;
            state.burnwire_secs[0] = 0;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_BURNWIRE_OFF_1:
            state.wires_cmds[0] = 0;
            state.burnwire_secs[0] = 5;
            break;

          case sentry_msgs::XRCmdRequest::XR_CMD_DCAM_OPEN_2:
            state.dcams_cmds[1] = 1;
            state.motor_secs[0] = 5;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_DCAM_CLOSE_2:
            state.dcams_cmds[1] = 0;
            state.motor_secs[0] = 5;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_BURNWIRE_ON_2:
            state.wires_cmds[1] = 1;
            state.burnwire_secs[1] = 3600;
            break;
          case sentry_msgs::XRCmdRequest::XR_CMD_BURNWIRE_OFF_2:
            state.wires_cmds[1] = 0;
            state.wires_actual[1] = 0;
            state.burnwire_secs[1] = 0;
            break;
          default:
            ROS_ERROR_STREAM("Unknown XR Request Command" <<req.command);
        }

        return true;
    }

    void OnUpdate(const gazebo::common::UpdateInfo& _info, double _fluid_aoa, double _fluid_vel) {

        // update our time
        if (initializing) {
            // don't update if its our first timestep
            initializing = false;
            lastSimTime = _info.simTime;
            return;
        }

        state.stamp = ros::Time().now();
        gazebo::common::Time gz_dt = _info.simTime - lastSimTime;
        lastSimTime = _info.simTime;
        double dt = gz_dt.Double();

        // only update state internally if we don't have hardware
        if (!hardware_in_loop) {

            // update our deadmen
            state.short_deadsecs -= dt;
            state.deadsecs -= dt;
            if (state.deadsecs <= 0 || state.short_deadsecs <= 0) {
              state.motor_secs[0] = 5.0;
              state.motor_secs[1] = 5.0;
              state.burnwire_secs[0] = 3600.0;
              state.burnwire_secs[1] = 3600.0;
              state.deadsecs = std::max<float>(0, state.deadsecs);
              state.short_deadsecs = std::max<float>(0, state.short_deadsecs);
            }
    
            // update our open / burn percentages
            for (size_t i=0; i<state.dcam_open_pct.size(); i++) {
              if (state.motor_secs[i] > 0) {
    
                state.motor_secs[i] -= dt;
                state.dcams_actual[i] = state.dcams_cmds[i];
    
                if (state.dcams_actual[i]) {
                  // Close the dcams (actual=1 --> open)
                  clamp(state.dcam_open_pct[i] += (100.0 / dcamtime * dt));
                } else {
                  // Open the dcams (actual=0 --> close)
                  clamp(state.dcam_open_pct[i] -= (100.0 / dcamtime * dt));
                }
              } else {
                state.motor_secs[i] = 0;
              }
            }
    
            // burn wires only go one way
            for (size_t i=0; i<state.wire_burn_pct.size(); i++) {
              if (state.burnwire_secs[i] > 0) {
    
                state.burnwire_secs[i] -= dt;
                state.wires_actual[i] = state.wires_cmds[i];
    
                if (state.wires_actual[i]) {
                  // actual=1 -> burn baby burn
                  clamp(state.wire_burn_pct[i] += (100.0 / burntime * dt));
                }
              } else {
                state.burnwire_secs[i] = 0;
              }
            }
        }

        // publish!
        if (_info.simTime > lastRosPublished + rosPublishPeriod) {
            rosSimPub.publish(state);
            lastRosPublished = _info.simTime;

            if (!hardware_in_loop) {
              ds_hotel_msgs::XR msg;
              msg.header.stamp = state.stamp;
              msg.ds_header.io_time = state.stamp;

              msg.deadhour = static_cast<int>(state.deadsecs / 3600.0);
              msg.good = true;
              msg.address = sail_address;
              msg.idnum = state.idx;

              msg.C = state.acoustic_code;

              msg.V = 0; // ??

              // drop motors
              msg.motor_1_drop = state.motor_secs[0] > 0 && state.dcams_actual[0] == true;
              msg.motor_1_hold = state.motor_secs[0] > 0 && state.dcams_actual[0] == false;
              msg.motor_2_drop = state.motor_secs[1] > 0 && state.dcams_actual[1] == true;
              msg.motor_2_hold = state.motor_secs[1] > 0 && state.dcams_actual[1] == false;

              msg.burnwire_1_drive = state.burnwire_secs[0] > 0 && state.wires_actual[0] == true;
              msg.burnwire_2_drive = state.burnwire_secs[1] > 0 && state.wires_actual[1] == true;

              msg.motor_1_secs = static_cast<uint16_t>(state.motor_secs[0]);
              msg.motor_2_secs = static_cast<uint16_t>(state.motor_secs[1]);

              msg.burnwire_1_secs = static_cast<uint16_t>(state.burnwire_secs[0]);
              msg.burnwire_2_secs = static_cast<uint16_t>(state.burnwire_secs[1]);

              msg.short_deadsecs = static_cast<uint8_t>(state.short_deadsecs);
              msg.short_deadsecs_idle = 255;

              msg.deadsecs = static_cast<int32_t>(state.deadsecs);

              rosPub.publish(msg);
            }
        }
    }

    void command_open_all_dcams() {
        for (size_t i=0; i<state.dcams_cmds.size(); i++) {
            state.dcams_cmds[i] = false;
        }
        open_all_dcams();
    }

    void open_all_dcams() {
        for (size_t i=0; i<state.dcams_actual.size(); i++) {
            state.dcams_actual[i] = false;
        }
    }

    void command_burn_all_wires() {
        for (size_t i=0; i<state.wires_cmds.size(); i++) {
            state.wires_cmds[i] = true;
        }
    }
    void burn_all_wires() {
        for (size_t i=0; i<state.wires_actual.size(); i++) {
            state.wires_actual[i] = true;
        }
    }

    float& clamp(float& value) {
        if (value < 0.0) {
            value = 0.0;
        }
        if (value > 100.0) {
            value = 100.0;
        }
        return value;
    }

    void updateWeightVector(std::vector<bool>& weights) {
        // check dcams
        for (size_t i=0; i<state.dcam_open_pct.size(); i++) {
            if (state.dcam_open_pct[i] >= 100.0) {
                if (dcam_idx[i] > 0 && dcam_idx[i] <= weights.size()) {
                    weights[dcam_idx[i]-1] = false;
                } else if (dcam_idx[i] >= 0) {
                    ROS_ERROR_STREAM("Attempted to drop weight #" <<dcam_idx[i] <<", which is not a real weight!");
                }
            }
        }
        // check burn wires
        for (size_t i=0; i<state.wire_burn_pct.size(); i++) {
            if (state.wire_burn_pct[i] >= 100.0) {
                if (burn_idx[i] > 0 && burn_idx[i] <= weights.size()) {
                    weights[burn_idx[i]-1] = false;
                } else if (burn_idx[i] >= 0) {
                    ROS_ERROR_STREAM("Attempted to drop weight #" <<burn_idx[i] <<", which is not a real weight!");
                }
            }
        }
    }

  protected:
    ds_hotel_msgs::XrSimState state;
    float longman; //longman timer-- 
    float shortman_reset;
    float burntime, dcamtime; // Total time, in seconds
    bool initializing;
    std::vector<int> burn_idx, dcam_idx;
    std::string sim_param_ns;
    std::string sail_address;
    bool hardware_in_loop;
    gazebo::common::Time lastSimTime;
    gazebo::common::Time rosPublishPeriod;
    gazebo::common::Time lastRosPublished;

    ros::Subscriber rosAbortSub;
    ros::Subscriber rosRealXrSub;
    ros::Publisher rosPub;
    ros::Publisher rosSimPub;
    ros::ServiceServer rosServe;
};


// plugins MUST be in the gazebo namespace
namespace gazebo {
class SentryDynamics : public ModelPlugin {
  public:
    SentryDynamics() {}

    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) {
        this->model = _parent;

        // Check on ROS
        if (!ros::isInitialized()) {
            ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
                << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
            return;
        }

        // load standard objects
        std::string root_topic_name;
        if (! (loadString(root_topic_name, _sdf, "root_topic")
            && loadDouble(min_depth, _sdf, "min_depth")
            && loadString(body_link , _sdf, "linkname"))) {
            return;
        }

        // get the namespace and setup ROS
        std::string robotNamespace;
        if (_sdf->HasElement("robotNamespace")) {
            robotNamespace = _sdf->Get<std::string>("robotNamespace");
        } else {
            robotNamespace = _parent->GetName();
        }
        this->rosNode.reset(new ros::NodeHandle(robotNamespace));

        // Setup the update rate
        double updateRate = 10;
        if (_sdf->HasElement("updateRate")) {
            updateRate = _sdf->Get<double>("updateRate");
        }
        rosPublishPeriod = gazebo::common::Time(1.0/updateRate);
        lastRosPublished = gazebo::common::Time(0.0);
        lastSimTime = gazebo::common::Time(0.0);
        initializing = true;

        // default maximum z velocity (abs)
        MAX_Z_VELOCITY = 3.0;
        if (_sdf->HasElement("maxZvel")) {
            MAX_Z_VELOCITY = _sdf->Get<double>("maxZvel");
        }

        // default minimum x/z velocity (abs, m/s)
        MIN_XZ_VELOCITY = 0.00001;
        if (_sdf->HasElement("minXZvel")) {
            MIN_XZ_VELOCITY = _sdf->Get<double>("minXZvel");
        }

        // Load the model
        vehicle_model.Load(_parent, _sdf->GetElement("model"));

        // Load the servos
        servos.resize(2);
        servos[IDX_SERVO_FWD].Load(&vehicle_model, rosNode, _parent, _sdf->GetElement("servo_fwd"));
        servos[IDX_SERVO_AFT].Load(&vehicle_model, rosNode, _parent, _sdf->GetElement("servo_aft"));

        // Load the thrusters
        thrusters.resize(4);
        thrusters[IDX_THRUSTER_PF].Load(&vehicle_model, rosNode, _parent, _sdf->GetElement("thruster_fwd_port"));
        thrusters[IDX_THRUSTER_SF].Load(&vehicle_model, rosNode, _parent, _sdf->GetElement("thruster_fwd_stbd"));
        thrusters[IDX_THRUSTER_PA].Load(&vehicle_model, rosNode, _parent, _sdf->GetElement("thruster_aft_port"));
        thrusters[IDX_THRUSTER_SA].Load(&vehicle_model, rosNode, _parent, _sdf->GetElement("thruster_aft_stbd"));

        thrusters[IDX_THRUSTER_PF].setServo(&servos[IDX_SERVO_FWD]);
        thrusters[IDX_THRUSTER_SF].setServo(&servos[IDX_SERVO_FWD]);
        thrusters[IDX_THRUSTER_PA].setServo(&servos[IDX_SERVO_AFT]);
        thrusters[IDX_THRUSTER_SA].setServo(&servos[IDX_SERVO_AFT]);

        // advertise simulated state setup
        rosPub = rosNode->advertise<sentry_msgs::SimState>(root_topic_name + "/state", 10);
        rosPubFins  = rosNode->advertise<geometry_msgs::WrenchStamped>(root_topic_name + "/fins", 10);
        rosPubThrust= rosNode->advertise<geometry_msgs::WrenchStamped>(root_topic_name + "/thrust", 10);
        rosPubDrag  = rosNode->advertise<geometry_msgs::WrenchStamped>(root_topic_name + "/drag", 10);
        rosPubWeight= rosNode->advertise<geometry_msgs::WrenchStamped>(root_topic_name + "/weight", 10);

        // Load the dropweights
        int weights;
        if (! (loadElement(N_buoyancy, _sdf, "buoyancy_N")
            && loadElement(N_per_stack, _sdf, "dropweight_N")
            && loadElement(weights, _sdf, "num_dropweights"))) {
            return;
        }
        weights_attached.resize(weights, true);

        xrs.resize(2);
        xrs[IDX_XR_1].Load(&vehicle_model, rosNode, _parent, _sdf->GetElement("xr1"));
        xrs[IDX_XR_2].Load(&vehicle_model, rosNode, _parent, _sdf->GetElement("xr2"));

        // Load some ROS parameters
        bool load_descent;
        rosNode->getParam(root_topic_name + "/load_descent", load_descent);

        if (!load_descent) {
          // command the actuator to open
          ROS_WARN_STREAM("Dropping descent weight on command of parameter server...");
          ROS_WARN_STREAM("If you want a descent weight, try setting " <<rosNode->resolveName(root_topic_name + "/load_descent"));
          xrs[IDX_XR_2].ForceOpenDcam(0);
          // go ahead and magically drop our weight
          weights_attached[0] = false;
        } else {
          ROS_WARN_STREAM("Loading descent weight on command of parameter server...");
        }

        bool load_ascent;
        rosNode->getParam(root_topic_name + "/load_ascent",  load_ascent);
        if (!load_descent) {
          // command the actuator to open
          ROS_WARN_STREAM("Dropping ascent weight on command of parameter server...");
          ROS_WARN_STREAM("If you want ascent weights, try setting " <<rosNode->resolveName(root_topic_name + "/load_ascent"));
          xrs[IDX_XR_1].ForceOpenDcam(1);
          xrs[IDX_XR_2].ForceOpenDcam(0);
          // go ahead and magically drop our weight
          weights_attached[1] = false;
          weights_attached[2] = false;
        } else {
          ROS_WARN_STREAM("Loading descent weight on command of parameter server...");
        }
        // Listen to the update event
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&SentryDynamics::OnUpdate, this, _1));
    }

    void OnUpdate(const common::UpdateInfo& _info) {

        // (possibly) update the state
        if (initializing) {
            initializing = false;
            ROS_INFO("Initializing SENTRY state...");
            initState(_info);
        } else {
            updateState(_info);
        }

        // We're gonna need a center-of-gravity later
        physics::LinkPtr canon_link = model->GetLink();
        physics::InertialPtr canon_inertial = canon_link->GetInertial();
	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Vector3d cog = canon_inertial->CoG();
	    ignition::math::Pose3d gz_pose;
        #else
	    math::Vector3 cog = canon_inertial->GetCoG();
	    math::Pose gz_pose;
	#endif
	
        fossen2gazebo_pose(gz_pose, state.pose);
	
	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Quaterniond gz_att = gz_pose.Rot();
	#else
	    math::Quaternion gz_att = gz_pose.rot;
	#endif
    
        //model->SetWorldPose(gz_pose);

        // Some sensors require actual accelerations / velocities
        // to work properly (e.g,. the Phins)

        // First: Angular velocity + acceleration
        // Our state vector has everything in body coordinates.  Gazebo
        // does everything in global coordinates
	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Vector3d gz_ang_vel;
            ignition::math::Vector3d gz_body_ang_vel(state.velocity.angular.x,
                                                    -state.velocity.angular.y,
                                                    -state.velocity.angular.z);
            body2world_rates(gz_ang_vel, gz_att, gz_body_ang_vel);

            ignition::math::Vector3d gz_ang_acc;
            ignition::math::Vector3d gz_body_ang_acc(state.acceleration.angular.x,
                                                     -state.acceleration.angular.y,
                                                     -state.acceleration.angular.z);
            body2world_rates(gz_ang_acc, gz_att, gz_body_ang_acc);

            // NEXT: Linear velocity + acceleration
            // two-step process:
            // 1). Convert SNAME Fwd-Stbd-Down body-frame to Fwd-Port-Up body frame
            // 2). Kinematic correction to compute velocity of COG, because that's what gazebo wants (Grrrr)
            // 3). Using the gazebo body orientation, rotate body frame to world frame
            ignition::math::Vector3d gz_body_lin_vel = ignition::math::Vector3d(state.velocity.linear.x,
                                                                                -state.velocity.linear.y,
                                                                                -state.velocity.linear.z);
            ignition::math::Vector3d gz_cog_body_lin_vel = gz_body_lin_vel + gz_body_ang_vel.Cross(cog);
            ignition::math::Vector3d gz_cog_lin_vel = gz_att.RotateVector(gz_cog_body_lin_vel);

            // same two-step process:
            // 1). SNAME Fwd-Stbd-Down body-frame to Fwd-Port-Up gazebo/ros style frame
            // 2). Using the gazebo body frame, rotate to world frame
            ignition::math::Vector3d gz_body_lin_acc = gz_att.RotateVector(ignition::math::Vector3d(state.acceleration.linear.x,
                                                                          -state.acceleration.linear.y,
                                                                          -state.acceleration.linear.z));
            ignition::math::Vector3d gz_cog_body_lin_acc = gz_body_lin_acc + gz_body_ang_acc.Cross(cog); // TODO: Need real centripital math
            ignition::math::Vector3d gz_cog_lin_acc = gz_att.RotateVector(gz_cog_body_lin_acc);
	#else
	    math::Vector3 gz_ang_vel;
            math::Vector3 gz_body_ang_vel(state.velocity.angular.x,
                                          -state.velocity.angular.y,
                                          -state.velocity.angular.z);
            body2world_rates(gz_ang_vel, gz_att, gz_body_ang_vel);


            math::Vector3 gz_ang_acc;
            math::Vector3 gz_body_ang_acc(state.acceleration.angular.x,
                                          -state.acceleration.angular.y,
                                          -state.acceleration.angular.z);
            body2world_rates(gz_ang_acc, gz_att, gz_body_ang_acc);

            // NEXT: Linear velocity + acceleration
            // two-step process:
            // 1). Convert SNAME Fwd-Stbd-Down body-frame to Fwd-Port-Up body frame
            // 2). Kinematic correction to compute velocity of COG, because that's what gazebo wants (Grrrr)
            // 3). Using the gazebo body orientation, rotate body frame to world frame
            math::Vector3 gz_body_lin_vel = math::Vector3(state.velocity.linear.x,
                                                          -state.velocity.linear.y,
                                                          -state.velocity.linear.z);
            math::Vector3 gz_cog_body_lin_vel = gz_body_lin_vel + gz_body_ang_vel.Cross(cog);
            math::Vector3 gz_cog_lin_vel = gz_att.RotateVector(gz_cog_body_lin_vel);

            // same two-step process:
            // 1). SNAME Fwd-Stbd-Down body-frame to Fwd-Port-Up gazebo/ros style frame
            // 2). Using the gazebo body frame, rotate to world frame
            math::Vector3 gz_body_lin_acc = gz_att.RotateVector(math::Vector3(state.acceleration.linear.x,
                                                                       -state.acceleration.linear.y,
                                                                       -state.acceleration.linear.z));
            math::Vector3 gz_cog_body_lin_acc = gz_body_lin_acc + gz_body_ang_acc.Cross(cog); // TODO: Need real centripital math
            math::Vector3 gz_cog_lin_acc = gz_att.RotateVector(gz_cog_body_lin_acc);
	#endif


        // Gazebo, because it insists on being as annoying as possible, uses the center of mass for velocities
        // instead of the link origin

        //ROS_ERROR_STREAM("Setting pose: " <<gz_pose.pos.z <<" vel: " <<gz_cog_lin_vel.z);

        model->ResetPhysicsStates();
        model->SetWorldPose(gz_pose);
        model->SetLinearVel(gz_cog_lin_vel);
        model->SetAngularVel(gz_ang_vel);

    #if GAZEBO_MAJOR_VERSION < 9
        ROS_ERROR_STREAM_ONCE("GAZEBO_MAJOR_VERSION: "<<GAZEBO_MAJOR_VERSION<<", using SetVel Fxns");
        model->SetLinearAccel(gz_cog_lin_acc);
        model->SetAngularAccel(gz_ang_acc);
    #else
        ROS_ERROR_STREAM_ONCE("GAZEBO_MAJOR_VERSION: "<<GAZEBO_MAJOR_VERSION<<", using SetForce Fxns");
        canon_link->AddForce(gz_cog_lin_acc);
        canon_link->AddRelativeForce(gz_ang_acc);

        //Easy debug tool to get all model links	
        //std::vector<physics::LinkPtr> links = model->GetLinks();  for (int i = 0; i < links.size(); ++i)  {  ROS_ERROR("Link[%d]: %s", i, links[i]->GetName().c_str()); }
    #endif

        // publish a state message
        if (_info.simTime > lastRosPublished + rosPublishPeriod) {
            rosPub.publish(state);
            lastRosPublished = _info.simTime;

            geometry_msgs::WrenchStamped msg;
            msg.header.stamp = state.stamp;
            msg.header.frame_id = body_link;

            msg.wrench = state.fin_newtons;
            rosPubFins.publish(msg);

            msg.wrench = state.thrust_newtons;
            rosPubThrust.publish(msg);

            msg.wrench = state.drag_newtons;
            rosPubDrag.publish(msg);

            msg.wrench.force.x = 0;
            msg.wrench.force.y = 0;
            msg.wrench.force.z = -state.net_weight;
            msg.wrench.torque.x = 0;
            msg.wrench.torque.y = 0;
            msg.wrench.torque.z = 0;
            rosPubWeight.publish(msg);
        }


    }


    void initState(const common::UpdateInfo& _info) {
        state.stamp = ros::Time().now();
        state.dt = 0;
        lastSimTime = _info.simTime;

	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Quaterniond fpu2fsd(0,1,0,0);
            ned2enu = ignition::math::Quaterniond(0, 1.0/sqrt(2.0), 1.0/sqrt(2.0), 0);
            ignition::math::Pose3d gz_pose = model->WorldPose();
        #else
            math::Quaternion fpu2fsd(0,1,0,0);
            ned2enu = math::Quaternion(0, 1.0/sqrt(2.0), 1.0/sqrt(2.0), 0);
            math::Pose gz_pose = model->GetWorldPose();
	#endif
	
	
        // set pose
        gazebo2fossen_pose(state.pose, gz_pose);

        // initialize velocities to zero
        state.velocity.linear.x = 0;
        state.velocity.linear.y = 0;
        state.velocity.linear.z = 0;
        state.velocity.angular.x = 0;
        state.velocity.angular.y = 0;
        state.velocity.angular.z = 0;

        
        // acceleration is determined directly from force
        // force is calculated from stuff

        // set our initial net weight to 0; it will be updated by the XR
        // dropweights.
        state.net_weight = 0;
    }

    void updateState(const common::UpdateInfo& _info) {
        state.stamp = ros::Time().now();
        gazebo::common::Time dt = _info.simTime - lastSimTime;
        lastSimTime = _info.simTime;
        state.dt = dt.Double();

        // Recover vehicle state
	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Vector3d uvw(state.velocity.linear.x,
                              state.velocity.linear.y,
                              state.velocity.linear.z);
            ignition::math::Vector3d abc(state.velocity.angular.x,
                              state.velocity.angular.y,
                              state.velocity.angular.z);
        #else
            math::Vector3 uvw(state.velocity.linear.x,
                              state.velocity.linear.y,
                              state.velocity.linear.z);
            math::Vector3 abc(state.velocity.angular.x,
                              state.velocity.angular.y,
                              state.velocity.angular.z);
        #endif
	
        // Now, pre-compute some stuff we'll need for e.g., drag
        if (fabs(uvw[0]) < MIN_XZ_VELOCITY && fabs(uvw[2]) < MIN_XZ_VELOCITY) {
            state.body_aoa = 0.0;
        } else {
            state.body_aoa = atan2(uvw[2], uvw[0]);
        }
        state.body_vel = sqrt(uvw[0]*uvw[0] + uvw[2]*uvw[2]);

	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Vector3d fins_xyz (0.0, 0.0, 0.0);
            ignition::math::Vector3d fins_rph (0.0, 0.0, 0.0);
            ignition::math::Vector3d thrust_xyz (0.0, 0.0, 0.0);
            ignition::math::Vector3d thrust_rph(0.0, 0.0, 0.0);
	#else
	    math::Vector3 fins_xyz (0.0, 0.0, 0.0);
            math::Vector3 fins_rph (0.0, 0.0, 0.0);
            math::Vector3 thrust_xyz (0.0, 0.0, 0.0);
            math::Vector3 thrust_rph(0.0, 0.0, 0.0);
	#endif
        // Fill in servo / fin parameters
        state.servos.resize(servos.size());
        for (size_t i=0; i<servos.size(); i++) {
            servos[i].OnUpdate(_info, state.body_aoa, state.body_vel);
            state.servos[i] = servos[i].getSimState();
            fins_xyz  += servos[i].getForceXYZ();
            fins_rph += servos[i].getTorqueRPH();
        }
        fillWrenchFossen(state.fin_newtons, fins_xyz, fins_rph);

        // Now the thrusters
        state.thrusters.resize(thrusters.size());
        for (size_t i=0; i<thrusters.size(); i++) {
            thrusters[i].OnUpdate(_info, state.body_aoa, state.body_vel);
            state.thrusters[i] = thrusters[i].getSimState();
            thrust_xyz  += thrusters[i].getForceXYZ();
            thrust_rph += thrusters[i].getTorqueRPH();
        }
        fillWrenchFossen(state.thrust_newtons, thrust_xyz, thrust_rph);

        // compute drag
	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Vector3d drag_xyz(-vehicle_model.getCdu()*uvw[0]*fabs(uvw[0]),
                                               0,
                                               -vehicle_model.getCdw()*uvw[2]*fabs(uvw[2]));

	    ignition::math::Vector3d drag_rph(  0,
                                               0,
                                               -vehicle_model.getCdr()*abc[2]*fabs(abc[2]));
	#else
	    math::Vector3 drag_xyz(-vehicle_model.getCdu()*uvw[0]*fabs(uvw[0]),
                                   0,
                                   -vehicle_model.getCdw()*uvw[2]*fabs(uvw[2]));

            math::Vector3 drag_rph(  0,
                                     0,
                                   -vehicle_model.getCdr()*abc[2]*fabs(abc[2]));
	#endif
        fillWrenchFossen(state.drag_newtons, drag_xyz, drag_rph);

        // Check the XRs and the dropweights
        state.net_weight = -N_buoyancy;
        for (Xr& xr : xrs) {
            xr.OnUpdate(_info, state.body_aoa, state.body_vel);
            xr.updateWeightVector(weights_attached);
        }
        state.dropweightsAttached.clear();
        state.dropweightsAttached.reserve(weights_attached.size());
        for (bool w : weights_attached) {
            if (w) {
                state.net_weight += N_per_stack;
            }
            state.dropweightsAttached.push_back(w);
        }

	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Vector3d weight(0,0,state.net_weight);
	#else
            math::Vector3 weight(0,0,state.net_weight);
	#endif
	    
        // sum forces and divide by mass / inertia to get acceleration
	#if GAZEBO_MAJOR_VERSION > 7
	    ignition::math::Vector3d total_xyz = fins_xyz + thrust_xyz + drag_xyz + weight;
            ignition::math::Vector3d total_rph = fins_rph + thrust_rph + drag_rph;
	#else
	    math::Vector3 total_xyz = fins_xyz + thrust_xyz + drag_xyz + weight;
            math::Vector3 total_rph = fins_rph + thrust_rph + drag_rph;
	#endif

        state.acceleration.linear.x = total_xyz[0]/(vehicle_model.getMu());
        state.acceleration.linear.y = 0.0;
        state.acceleration.linear.z = total_xyz[2]/(vehicle_model.getMw());
        state.acceleration.angular.x = 0.0;
        state.acceleration.angular.y = 0.0;
        state.acceleration.angular.z = total_rph[2]/(vehicle_model.getIzz());

        // integrate! to get velocity
        state.velocity.linear.x += state.acceleration.linear.x*state.dt;
        state.velocity.linear.y += state.acceleration.linear.y*state.dt;
        state.velocity.linear.z += state.acceleration.linear.z*state.dt;
        state.velocity.angular.x += state.acceleration.angular.x*state.dt;
        state.velocity.angular.y += state.acceleration.angular.y*state.dt;
        state.velocity.angular.z += state.acceleration.angular.z*state.dt;

        // clamp z-velocity
        if (state.velocity.linear.z > MAX_Z_VELOCITY) {
            state.velocity.linear.z = MAX_Z_VELOCITY;
        }
        if (state.velocity.linear.z < -MAX_Z_VELOCITY) {
            state.velocity.linear.z = -MAX_Z_VELOCITY;
        }

        // clamp x and z velocities to zero if they're small
        if (fabs(state.velocity.linear.x) < MIN_XZ_VELOCITY) {
            state.velocity.linear.x = 0.0;
        }
        if (fabs(state.velocity.linear.z) < MIN_XZ_VELOCITY) {
            state.velocity.linear.z = 0.0;
        }

        // we have to convert to global coordinates when we do this.  So, you know.  Make it happen.
        double yaw = tf::getYaw(state.pose.orientation);
        double ch = cos(yaw);
        double sh = sin(yaw);

        state.pose.position.x += ( (state.velocity.linear.x*ch - state.velocity.linear.y*sh) * state.dt);
        state.pose.position.y += ( (state.velocity.linear.x*sh + state.velocity.linear.y*ch) * state.dt);
        state.pose.position.z += (  state.velocity.linear.z * state.dt);
        yaw += (state.velocity.angular.z * state.dt);
        //ROS_INFO_STREAM("\tOrig Yaw: " <<yaw*180/M_PI);

        // If we're as shallow as we're allowed to get, clamp all
        // z state variables to only let us go down
        if (state.pose.position.z <= min_depth) {
            if (state.acceleration.linear.z < 0) {
                state.acceleration.linear.z = 0;
            }
            if (state.velocity.linear.z < 0) {
                state.velocity.linear.z = 0;
            }
            state.pose.position.z = min_depth;
        }

        // compute our orientation quaternion
        state.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0,0,yaw);
    }

    const VehicleModel& getModel() const { return vehicle_model; }

  protected:
    physics::ModelPtr model;
    //physics::LinkPtr base_link;
    event::ConnectionPtr updateConnection;
    bool initializing;

    // flip x/y, invert z
    #if GAZEBO_MAJOR_VERSION > 7
    ignition::math::Quaterniond ned2enu;
    #else
    math::Quaternion ned2enu;
    #endif

    std::unique_ptr<ros::NodeHandle> rosNode;
    ros::Publisher rosPub;
    sentry_msgs::SimState state;
    common::Time rosPublishPeriod, lastRosPublished, lastSimTime;
    double MAX_Z_VELOCITY;
    double MIN_XZ_VELOCITY;
    double min_depth;

    ros::Publisher rosPubFins, rosPubThrust, rosPubDrag, rosPubWeight;
    std::string body_link;

    const static int IDX_SERVO_FWD = 0;
    const static int IDX_SERVO_AFT = 1;
    std::vector<SentryServo> servos;

    const static int IDX_THRUSTER_PF = 0;
    const static int IDX_THRUSTER_SF = 1;
    const static int IDX_THRUSTER_PA = 2;
    const static int IDX_THRUSTER_SA = 3;
    std::vector<SentryThruster> thrusters;

    const static int IDX_XR_1 = 0;
    const static int IDX_XR_2 = 1;
    std::vector<Xr> xrs;
    double N_buoyancy;
    double N_per_stack;
    std::vector<bool> weights_attached;

    VehicleModel vehicle_model;
};

GZ_REGISTER_MODEL_PLUGIN(SentryDynamics)
}; // namespace gazebo

